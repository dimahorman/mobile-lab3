import Axios from 'axios-observable';
import {AxiosObservable} from "axios-observable/dist/axios-observable.interface";
import {environments} from "../config/environment";
import {Farmer} from "../data/Farmer";
import {Farm} from "../data/Farm";
import {FarmField} from "../data/FarmField";
import {Sensor} from "../data/Sensor";
import {Item, ItemShortInfo} from "../data/Item";
import axios, {AxiosInstance, AxiosResponse} from "axios";

export class ApiService {
    constructor() {
    }

    public static getInstance(token: string): AxiosInstance {
        return axios.create({
            baseURL: environments.url,
            headers: {'Authorization': 'ApiKey ' + token}
        });
    }

    public getFarmer(name: string, axiosInstance: AxiosInstance): Promise<AxiosResponse<Farmer>> {
        return axiosInstance.get<Farmer>('/farmers/' + name)
    }

    public getFarmersNames(axiosInstance: AxiosInstance): Promise<AxiosResponse<Array<string>>> {
        return axiosInstance.get<Array<string>>('/farmers/names');
    }

    public getFarmsIdsByFarmerName(farmerName: string, axiosInstance: AxiosInstance): Promise<AxiosResponse<Array<string>>> {
        return axiosInstance.get<Array<string>>('/farmers/' + farmerName + '/farms/ids')
    }

    public getFarmById(id: string, axiosInstance: AxiosInstance): Promise<AxiosResponse<Farm>> {
        return axiosInstance.get<Farm>('/farms/' + id)
    }

    public getFarmFieldsIdsByFarmId(farmId: string, axiosInstance: AxiosInstance): Promise<AxiosResponse<Array<number>>> {
        return axiosInstance.get<Array<number>>('/farms/' + farmId + '/fields/ids')
    }

    public getFarmField(id: number, axiosInstance: AxiosInstance): Promise<AxiosResponse<FarmField>> {
        return axiosInstance.get<FarmField>('/fields/' + id)
    }

    public getSensorsIdsByFarmFieldId(fieldId: number, axiosInstance: AxiosInstance): Promise<AxiosResponse<Array<number>>> {
        return axiosInstance.get<Array<number>>('/fields/' + fieldId + '/sensors/ids')
    }

    public getSensor(id: number, axiosInstance: AxiosInstance): Promise<AxiosResponse<Sensor>> {
        return axiosInstance.get<Sensor>('/sensors/' + id)
    }

    public getItemsIds(axiosInstance: AxiosInstance): Promise<AxiosResponse<Array<string>>> {
        return axiosInstance.get<Array<string>>('/items/ids')
    }

    public getItemById(id: string, axiosInstance: AxiosInstance): Promise<AxiosResponse<Item>> {
        return axiosInstance.get<Item>('/items/' + id)
    }

    public addItem(itemShortInfo: ItemShortInfo, axiosInstance: AxiosInstance): Promise<AxiosResponse> {
        return axiosInstance.post('/items/', itemShortInfo)
    }
}
