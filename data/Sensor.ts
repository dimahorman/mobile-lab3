export interface Sensor {
    id: number;
    fieldId: number;
    gpsCoordinates: string;
    measuredValue: string;
    type: SensorType;
}

export const enum SensorType {
    HUMIDITY = 'Humidity',
    LIGHTING = 'Lighting',
    NONE = 'None'
}
