export interface FarmField {
    id: number;
    farmId: string;
    type: FarmFieldType;
}

export const enum FarmFieldType {
    BANANA = 'Banana',
    CUCUMBER = 'Cucumber',
    POTATO = 'Potato',
    CORN = 'Corn',
    NONE = 'None'
}
