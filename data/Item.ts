export interface Item {
    id: string;
    userId: string;
    coordinates: string;
    height: number;
    material: string;
    weight: number;
}

export interface ItemShortInfo {
    coordinates: string;
    height: number;
    material: string;
    weight: number;
}
