import React from 'react';
import {Button, SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import {createDrawerNavigator, DrawerItemList} from '@react-navigation/drawer';
import {FarmersNavigator} from "./home/FarmersNavigator";
import {Settings} from "./settings/Settings";
import firebase from "../../config/firebase";
import {ItemsNavigator} from "./items/ItemsNavigator";
import {useTheme} from "@react-navigation/native";


const Drawer = createDrawerNavigator();


export default function UserNavigator() {
    const {colors} = useTheme();
    const CustomDrawerContentComponent = (props) => {
        const onPressLogOut = () => {
            firebase.auth().signOut().then(() => {
                props.navigation.reset({
                    index: 0,
                    routes: [{name: "Auth"}],
                });
            })
                .catch((error) => console.log(error.errorMessage))

        }
        const styles = () => {
            return {
                sideBar: {
                    marginTop: 70,
                },
                title: {
                    fontSize: 27,
                    color: colors.text,
                }
            }
        }
        return (
            <ScrollView>
                <SafeAreaView style={styles().sideBar} forceInset={{top: 'always', horizontal: 'never'}}>
                    <Text style={styles().title}>Welcome, {firebase.auth().currentUser.displayName}</Text>
                    <Button onPress={onPressLogOut} title={"Log Out"}/>
                    <DrawerItemList  {...props} />
                </SafeAreaView>
            </ScrollView>
        );
    }
    return (
        <Drawer.Navigator drawerContent={CustomDrawerContentComponent}
                          initialRouteName={"Items"}>
            <Drawer.Screen name="Items" component={ItemsNavigator}/>
            <Drawer.Screen name="Farmers" component={FarmersNavigator}/>
            <Drawer.Screen name="Settings" component={Settings}/>
        </Drawer.Navigator>


    );
}

