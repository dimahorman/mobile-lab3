import React, {useEffect, useState} from "react";
import {Button, FlatList, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {StyleSheetFactory} from "../../../../App";
import {useTheme} from "@react-navigation/native";
import firebase from "../../../../config/firebase";
import {ApiService} from "../../../../service/ApiService";

const db = firebase.firestore();
const apiService = new ApiService();

export function FarmFieldComponent({navigation, route}) {
    const {colors} = useTheme();
    const [farmFieldsIds, setFarmFieldsIds] = useState([]);
    const [userId, setUserId] = useState(firebase.auth().currentUser.uid);
    const {farmId} = route.params;
    let onSnapshotUnsubscribe;
    let styles = StyleSheetFactory.getSheet(colors)

    const getFarmFieldsIds = () => {
        firebase.auth().currentUser.getIdToken(false).then((res) => {
            const instance = ApiService.getInstance(res);
            apiService.getFarmFieldsIdsByFarmId(farmId, instance).then((resp) => {
                console.log(resp.data);
                setFarmFieldsIds(resp.data)
            });
        });
    };

    const Item = ({title, style}) => {
        const [isSelected, setIsSelected] = useState(false)
        const [itemData, setItemData] = useState(null)
        const onPressButton = () => {
            navigation.navigate('Sensors', {farmFieldId: title});
        };
        const onPress = () => {
            if (isSelected) {
                setIsSelected(!isSelected);
            } else {
                fetchData();
            }
        };

        const fetchData = () => {
            firebase.auth().currentUser.getIdToken(false).then((res) => {
                const instance = ApiService.getInstance(res);
                apiService.getFarmField(title, instance).then((resp) => {
                    setItemData(resp.data);
                    setIsSelected(!isSelected);
                });
            });
        };

        const itemDetails = () => (
            <View style={styles.itemDetails}>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Id: </Text><Text style={styles.detailValue}>{itemData.id}</Text></View>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Farm Id: </Text><Text style={styles.detailValue}>{itemData.farmId}</Text></View>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Type: </Text><Text style={styles.detailValue}>{itemData.type}</Text></View>
                <Button title={"Sensors List"} onPress={onPressButton}/>
            </View>
        );
        useEffect(() => {
            return () => {
            }
        }, []);

        return (
            <View>
                <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
                    <Text style={styles.title}>{title}</Text>
                </TouchableOpacity>
                {isSelected && itemDetails()}
            </View>
        );
    }


    const renderItem = ({item}) => (
        <Item style={styles} title={item}/>
    );

    useEffect(() => {
        onSnapshotUnsubscribe =
            db.collection("app-db")
                .doc(userId)
                .onSnapshot((snapshot) => {
                    getFarmFieldsIds();
                });
        const onChangeStateEvent = navigation.addListener('state', payload => {
            getFarmFieldsIds();
        });
        const onFocusEvent = navigation.addListener('focus', payload => {
            getFarmFieldsIds();
        });
        return () => {
            if (onChangeStateEvent !== undefined && onFocusEvent !== undefined) {
                onChangeStateEvent.remove;
                onFocusEvent.remove;
            }
            if (onSnapshotUnsubscribe !== undefined) {
                onSnapshotUnsubscribe();
            }
        }
    }, []);
    return (
        <ScrollView>
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={farmFieldsIds}
                    renderItem={renderItem}
                    keyExtractor={item => item}/>
            </SafeAreaView>
        </ScrollView>
    )
}
