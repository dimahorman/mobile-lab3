import React, {useEffect, useState} from "react";
import {Button, FlatList, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {useTheme} from "@react-navigation/native";
import {StyleSheetFactory} from "../../../../App";
import firebase from "../../../../config/firebase";
import {ApiService} from "../../../../service/ApiService";

const db = firebase.firestore();
const apiService = new ApiService();

export function FarmComponent({navigation, route}) {
    const [farmsIds, setFarmsIds] = useState([]);
    const [userId, setUserId] = useState(firebase.auth().currentUser.uid);
    const {farmerName} = route.params;
    const {colors} = useTheme();
    let onSnapshotUnsubscribe;
    let styles = StyleSheetFactory.getSheet(colors)
    const getFarmsIds = () => {
        firebase.auth().currentUser.getIdToken(false).then((res) => {
            const instance = ApiService.getInstance(res);
            apiService.getFarmsIdsByFarmerName(farmerName, instance).then((resp) => {
                console.log(resp.data);
                setFarmsIds(resp.data)
            });
        });
    };

    const Item = ({title, style}) => {
        const [isSelected, setIsSelected] = useState(false)
        const [itemData, setItemData] = useState(null)
        const onPressButton = () => {
            navigation.navigate('Farm Fields', {farmId: title});
        };
        const onPress = () => {
            if (isSelected) {
                setIsSelected(!isSelected);
            } else {
                fetchData();
            }
        };

        const fetchData = () => {
            firebase.auth().currentUser.getIdToken(false).then((res) => {
                const instance = ApiService.getInstance(res);
                apiService.getFarmById(title, instance).then((resp) => {
                    setItemData(resp.data);
                    setIsSelected(!isSelected);
                });
            });
        };

        const itemDetails = () => (
            <View style={styles.itemDetails}>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Id: </Text><Text style={styles.detailValue}>{itemData.id}</Text></View>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Owner: </Text><Text
                    style={styles.detailValue}>{itemData.owner}</Text></View>
                <Button title={"Farm fields list"} onPress={onPressButton}/>
            </View>
        );
        useEffect(() => {
            return () => {
            }
        }, []);

        return (
            <View>
                <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
                    <Text style={styles.title}>{title}</Text>
                </TouchableOpacity>
                {isSelected && itemDetails()}
            </View>
        );
    }


    const renderItem = ({item}) => (
        <Item style={styles} title={item}/>
    );

    useEffect(() => {
        onSnapshotUnsubscribe =
            db.collection("app-db")
                .doc(userId)
                .onSnapshot((snapshot) => {
                    getFarmsIds();
                });
        const onChangeStateEvent = navigation.addListener('state', payload => {
            getFarmsIds();
        });
        const onFocusEvent = navigation.addListener('focus', payload => {
            getFarmsIds();
        });
        return () => {
            if (onChangeStateEvent !== undefined && onFocusEvent !== undefined) {
                onChangeStateEvent.remove;
                onFocusEvent.remove;
            }
            if (onSnapshotUnsubscribe !== undefined) {
                onSnapshotUnsubscribe();
            }
        }
    }, []);

    return (
        <ScrollView>
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={farmsIds}
                    renderItem={renderItem}
                    keyExtractor={item => item}/>
            </SafeAreaView>
        </ScrollView>
    )
}
