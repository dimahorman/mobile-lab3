import React, {useEffect, useState} from "react";
import {FlatList, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {useTheme} from "@react-navigation/native";
import {StyleSheetFactory} from "../../../../App";
import firebase from "../../../../config/firebase";
import {ApiService} from "../../../../service/ApiService";

const db = firebase.firestore();
const apiService = new ApiService();

export function SensorComponent({navigation, route}) {
    const [sensorsIds, setSensorsIds] = useState([]);
    const [userId, setUserId] = useState(firebase.auth().currentUser.uid);
    const {farmFieldId} = route.params;
    const {colors} = useTheme();
    let styles = StyleSheetFactory.getSheet(colors)
    let onSnapshotUnsubscribe;

    const getSensorsIds = () => {
        firebase.auth().currentUser.getIdToken(false).then((res) => {
            const instance = ApiService.getInstance(res);
            apiService.getSensorsIdsByFarmFieldId(farmFieldId, instance).then((resp) => {
                console.log(resp.data);
                setSensorsIds(resp.data)
            });
        });
    };

    const Item = ({title, style}) => {
        const [isSelected, setIsSelected] = useState(false)
        const [itemData, setItemData] = useState(null)

        const onPress = () => {
            if (isSelected) {
                setIsSelected(!isSelected);
            } else {
                fetchData();
            }
        };

        const fetchData = () => {
            firebase.auth().currentUser.getIdToken(false).then((res) => {
                const instance = ApiService.getInstance(res);
                apiService.getSensor(title, instance).then((resp) => {
                    setItemData(resp.data);
                    setIsSelected(!isSelected);
                });
            });
        };

        const itemDetails = () => (
            <View style={styles.itemDetails}>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Id: </Text><Text style={styles.detailValue}>{itemData.id}</Text></View>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Farm Field Id: </Text><Text
                    style={styles.detailValue}>{itemData.fieldId}</Text></View>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Gps Coordinates: </Text><Text
                    style={styles.detailValue}>{itemData.gpsCoordinates}</Text></View>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Type: </Text><Text
                    style={styles.detailValue}>{itemData.type}</Text></View>
                <View style={styles.detailText}><Text style={styles.detailHeader}>Measured
                    Value: </Text><Text style={styles.detailValue}>{itemData.measuredValue}</Text></View>
            </View>
        );
        useEffect(() => {
            return () => {
            }
        }, []);

        return (
            <View>
                <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
                    <Text style={styles.title}>{title}</Text>
                </TouchableOpacity>
                {isSelected && itemDetails()}
            </View>
        );
    }


    const renderItem = ({item}) => (
        <Item style={styles} title={item}/>
    );

    useEffect(() => {
        onSnapshotUnsubscribe =
            db.collection("app-db")
                .doc(userId)
                .onSnapshot((snapshot) => {
                    getSensorsIds();
                });
        const onChangeStateEvent = navigation.addListener('state', payload => {
            getSensorsIds();
        });
        const onFocusEvent = navigation.addListener('focus', payload => {
            getSensorsIds();
        });
        return () => {
            if (onChangeStateEvent !== undefined && onFocusEvent !== undefined) {
                onChangeStateEvent.remove;
                onFocusEvent.remove;
            }
            if (onSnapshotUnsubscribe !== undefined) {
                onSnapshotUnsubscribe();
            }
        }
    }, []);
    return (
        <ScrollView>
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={sensorsIds}
                    renderItem={renderItem}
                    keyExtractor={item => item}/>
            </SafeAreaView>
        </ScrollView>
    )
}
