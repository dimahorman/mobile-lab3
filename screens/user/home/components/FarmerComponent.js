import React, {useEffect, useState} from "react";
import {
    Text,
    View,
    SafeAreaView,
    FlatList,
    TouchableOpacity,
    Button,
    ScrollView
} from "react-native";
import {useTheme} from "@react-navigation/native";
import {StyleSheetFactory} from "../../../../App";
import firebase from "../../../../config/firebase";
import {ApiService} from "../../../../service/ApiService";

const apiService = new ApiService();
const db = firebase.firestore();

export function FarmerComponent({navigation}) {
    const [farmersNames, setFarmersNames] = useState([]);
    const {colors} = useTheme();
    const [userId, setUserId] = useState(firebase.auth().currentUser.uid);

    let onChangeStateEvent;
    let onFocusEvent;
    let onSnapshotUnsubscribe;
    let styles = StyleSheetFactory.getSheet(colors)
    const getFarmersNames = () => {
        firebase.auth().currentUser.getIdToken(false).then((res) => {
            const instance = ApiService.getInstance(res);
            apiService.getFarmersNames(instance).then((resp) => {
                console.log(resp.data);
                setFarmersNames(resp.data)
            });
        })
    };

    const Item = ({title, style}) => {
        const [isSelected, setIsSelected] = useState(false)
        const [itemData, setItemData] = useState(null)
        const onPressButton = () => {
            navigation.navigate('Farms', {farmerName: title});
        };
        const onPress = () => {
            if (isSelected) {
                setIsSelected(!isSelected);
            } else {
                fetchData();
            }
        };

        const fetchData = () => {
            firebase.auth().currentUser.getIdToken(false).then((res) => {
                const instance = ApiService.getInstance(res);
                apiService.getFarmer(title, instance).then((resp) => {
                    setItemData(resp.data);
                    setIsSelected(!isSelected);
                });
            });
        };

        const itemDetails = () => (
            <View style={style.itemDetails}>
                <View style={style.detailText}><Text
                    style={style.detailHeader}>Name: </Text><Text
                    style={style.detailValue}>{itemData.name}</Text></View>
                <View style={style.detailText}><Text
                    style={style.detailHeader}>Age: </Text><Text
                    style={style.detailValue}>{itemData.age}</Text></View>
                <View style={style.detailText}><Text style={style.detailHeader}>Birth
                    City: </Text><Text style={style.detailValue}>{itemData.birthCity}</Text></View>
                <View style={style.detailText}><Text style={style.detailHeader}>Skill
                    level: </Text><Text style={style.detailValue}>{itemData.skillLevel}</Text></View>
                <Button title={"Farms List"} onPress={onPressButton}/>
            </View>
        );
        useEffect(() => {
            return () => {
            }
        }, []);

        return (
            <View>
                <TouchableOpacity onPress={onPress} style={style.item}>
                    <Text style={style.title}>{title}</Text>
                </TouchableOpacity>
                {isSelected && itemDetails()}
            </View>
        );
    }


    const renderItem = ({item}) => (
        <Item style={styles} title={item}/>
    );

    useEffect(() => {
        onSnapshotUnsubscribe =
            db.collection("app-db")
                .doc(userId)
                .onSnapshot((snapshot) => {
                    getFarmersNames();
                });
        onChangeStateEvent = navigation.addListener('state', payload => {
            getFarmersNames();

        });
        onFocusEvent = navigation.addListener('focus', payload => {
            getFarmersNames();
        })
        return () => {
            if (onChangeStateEvent !== undefined && onFocusEvent !== undefined) {
                onChangeStateEvent.remove;
                onFocusEvent.remove;
            }

            if (onSnapshotUnsubscribe !== undefined) {
                onSnapshotUnsubscribe();
            }
        }
    }, []);

    return (
        <ScrollView>
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={farmersNames}
                    renderItem={renderItem}
                    keyExtractor={item => item}/>
            </SafeAreaView>
        </ScrollView>
    )
}


