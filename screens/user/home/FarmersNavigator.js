import {createStackNavigator} from "@react-navigation/stack";
import {FarmerComponent} from "./components/FarmerComponent";
import {FarmComponent} from "./components/FarmComponent";
import {FarmFieldComponent} from "./components/FarmFieldComponent";
import {SensorComponent} from "./components/SensorComponent";
import React from "react";

export function FarmersNavigator() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Farmers" component={FarmerComponent}/>
            <Stack.Screen name="Farms" component={FarmComponent}/>
            <Stack.Screen name="Farm Fields" component={FarmFieldComponent}/>
            <Stack.Screen name="Sensors" component={SensorComponent}/>
        </Stack.Navigator>
    )
}

const Stack = createStackNavigator();
