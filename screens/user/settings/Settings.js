import {ScrollView, Switch, Text, View} from "react-native";
import React, {useEffect, useState} from "react";
import {StyleSheetFactory} from "../../../App";
import firebase from "../../../config/firebase";
import {useTheme} from "@react-navigation/native";

export function Settings() {
    let onSnapshotUnsubscribe;
    const [isThemeSwitched, setIsThemeSwitched] = useState(false);
    const [userId, setUserId] = useState(firebase.auth().currentUser.uid);
    const { colors } = useTheme();
    const db = firebase.firestore();
    let styles = StyleSheetFactory.getSheet(colors)
    const onToggleSwitch = () => {
        db.collection("app-db")
            .doc(userId).set({
            isThemeSwitched: !isThemeSwitched
        });
    }
    useEffect(() => {
        db.collection("app-db")
            .doc(userId)
            .get()
            .then((snapshot) => {
                const data = snapshot.data()
                setIsThemeSwitched(data.isThemeSwitched);
            });

        onSnapshotUnsubscribe =
            db.collection("app-db")
                .doc(userId)
                .onSnapshot((snapshot) => {
                    const data = snapshot.data()
                    setIsThemeSwitched(data.isThemeSwitched);
                });

        return () => {
            if (onSnapshotUnsubscribe !== undefined) {
                onSnapshotUnsubscribe();
            }
        }
    }, [])

    return (
        <View style={styles.settingsContainer}>
            <Text style={styles.title}>Switch theme</Text>
            <Switch value={isThemeSwitched}
                    trackColor={{false: "rgb(255, 255, 255)", true: "rgb(50, 50, 50)"}}
                    onValueChange={onToggleSwitch}
            />
        </View>)
}
