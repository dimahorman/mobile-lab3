import {Button, Text, TextInput, View} from "react-native";
import React, {useEffect, useState} from "react";
import {StyleSheetFactory} from "../../../../App";
import {useTheme} from "@react-navigation/native";
import {ApiService} from "../../../../service/ApiService";
import firebase from "../../../../config/firebase";

const apiService = new ApiService();

export function AddItem({navigation}) {
    const {colors} = useTheme();
    const [coordinates, setCoordinates] = useState("");
    const [height, setHeight] = useState("");
    const [material, setMaterial] = useState("");
    const [weight, setWeight] = useState("");
    let styles = StyleSheetFactory.getSheet(colors)

    const onChangeCoordinates = (text) => {
        setCoordinates(text)
    }

    const onChangeHeight = (text) => {
        setHeight(text)
    }
    const onChangeMaterial = (text) => {
        setMaterial(text)
    }

    const onChangeWeight = (text) => {
        setWeight(text)
    }

    const onPressSubmit = () => {
        firebase.auth().currentUser.getIdToken(false).then((res) => {
            const instance = ApiService.getInstance(res);
            apiService.addItem(
                {
                    coordinates: coordinates,
                    height: parseInt(height),
                    material: material,
                    weight: parseInt(weight),
                },
                instance
            ).then((resp) => {
                navigation.reset({
                    index: 0,
                    routes: [{name: "Items list"}],
                });
            })
        });
    }

    useEffect(() => {
    });
    return (<View style={styles.inputContainer}>
        <Text style={{
            fontWeight: "800",
            fontSize: 30,
            color: colors.text
        }}>Add New Item</Text>

        <View style={{marginVertical: 40}}>
            <View>
                <Text style={styles.text}>Coordinates: </Text>
                <TextInput style={styles.textForm} value={coordinates}
                           onChangeText={text => onChangeCoordinates(text)}/>
            </View>
            <View>
                <Text style={styles.text}>Height: </Text>
                <TextInput keyboardType='numeric' style={styles.textForm} value={height}
                           onChangeText={text => onChangeHeight(text)}/>
            </View>
            <View>
                <Text style={styles.text}>Material: </Text>
                <TextInput style={styles.textForm} value={material} onChangeText={text => onChangeMaterial(text)}/>
            </View>
            <View>
                <Text style={styles.text}>Weight: </Text>
                <TextInput keyboardType='numeric' style={styles.textForm} value={weight}
                           onChangeText={text => onChangeWeight(text)}/>
            </View>
            <Button title={"Submit"} onPress={onPressSubmit}/>
        </View>
    </View>)
}
