import React, {useEffect, useState} from "react";
import {FlatList, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {useTheme} from "@react-navigation/native";
import firebase from "../../../../config/firebase";
import {StyleSheetFactory} from "../../../../App";
import {ApiService} from "../../../../service/ApiService";

const db = firebase.firestore();
const apiService = new ApiService();

export function ItemsList({navigation}) {

    const {colors} = useTheme();
    const [itemsIds, setItemsIds] = useState([]);
    const [userId, setUserId] = useState(firebase.auth().currentUser.uid);
    let onSnapshotUnsubscribe;
    let styles = StyleSheetFactory.getSheet(colors)

    const getItemsIds = () => {
        firebase.auth().currentUser.getIdToken(false).then((res) => {
            const instance = ApiService.getInstance(res);
            apiService.getItemsIds(instance).then((resp) => {
                console.log(resp.data);
                setItemsIds(resp.data)
            });
        });
    };

    const Item = ({title, style}) => {
        const [isSelected, setIsSelected] = useState(false)
        const [itemData, setItemData] = useState(null)
        const onPress = () => {
            if (isSelected) {
                setIsSelected(!isSelected);
            } else {
                fetchData();
            }
        };

        const fetchData = () => {
            firebase.auth().currentUser.getIdToken(false).then((res) => {
                const instance = ApiService.getInstance(res);
                apiService.getItemById(title, instance).then((resp) => {
                    setItemData(resp.data);
                    setIsSelected(!isSelected);
                });
            });
        };

        const itemDetails = () => (
            <View style={styles.itemDetails}>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Id: </Text><Text style={styles.detailValue}>{itemData.id}</Text></View>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>User Id: </Text><Text
                    style={styles.detailValue}>{itemData.userId}</Text></View>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Coordinates: </Text><Text
                    style={styles.detailValue}>{itemData.coordinates}</Text></View>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Height: </Text><Text
                    style={styles.detailValue}>{itemData.height}</Text></View>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Material: </Text><Text
                    style={styles.detailValue}>{itemData.material}</Text></View>
                <View style={styles.detailText}><Text
                    style={styles.detailHeader}>Weight: </Text><Text
                    style={styles.detailValue}>{itemData.weight}</Text></View>
            </View>
        );
        useEffect(() => {
            return () => {
            }
        }, []);

        return (
            <View>
                <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
                    <Text style={styles.title}>{title}</Text>
                </TouchableOpacity>
                {isSelected && itemDetails()}
            </View>
        );
    }


    const renderItem = ({item}) => (
        <Item style={styles} title={item}/>
    );

    useEffect(() => {
        onSnapshotUnsubscribe =
            db.collection("app-db")
                .doc(userId)
                .onSnapshot((snapshot) => {
                    getItemsIds();
                });
        const onChangeStateEvent = navigation.addListener('state', payload => {
            getItemsIds();
        });
        const onFocusEvent = navigation.addListener('focus', payload => {
            getItemsIds();
        });
        return () => {
            if (onChangeStateEvent !== undefined && onFocusEvent !== undefined) {
                onChangeStateEvent.remove;
                onFocusEvent.remove;
            }
            if (onSnapshotUnsubscribe !== undefined) {
                onSnapshotUnsubscribe();
            }
        }
    }, []);

    const onPressFloatingButton = () => {
        navigation.navigate('Add item')
    }

    return (

        <View>
            <ScrollView>
                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={itemsIds}
                        renderItem={renderItem}
                        keyExtractor={item => item}/>

                </SafeAreaView>
            </ScrollView>
            <TouchableOpacity
                onPress={onPressFloatingButton}
                style={styles.roundButton}>
                <Text style={styles.title}>+</Text>
            </TouchableOpacity>
        </View>
    )
}


