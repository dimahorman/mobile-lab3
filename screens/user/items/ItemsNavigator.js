import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import {ItemsList} from "./components/ItemsList";
import {AddItem} from "./components/AddItem";

export function ItemsNavigator() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Items list" component={ItemsList}/>
            <Stack.Screen name="Add item" component={AddItem}/>
        </Stack.Navigator>
    )
}

const Stack = createStackNavigator();
