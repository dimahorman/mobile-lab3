import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import {SignIn} from "./components/SignIn";
import {SignUp} from "./components/SignUp";

export function AuthNavigator() {
    return (
        <Stack.Navigator>
                <Stack.Screen name="Sign In" component={SignIn}/>
                <Stack.Screen name="Sign Up" component={SignUp}/>
            </Stack.Navigator>
    )
}

const Stack = createStackNavigator();
