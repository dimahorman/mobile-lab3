import React, {Component} from "react"
import {Alert, Button, StyleSheet, Text, TextInput, View} from "react-native"
import firebase from "../../../config/firebase";

export class SignUp extends Component {
    private readonly EMAIL_PLACEHOLDER: string = "Enter your Email";
    private readonly EMPTY_FIELD_ERROR_MSG: string = "This field cannot be empty"
    private readonly INVALID_EMAIL_ERROR_MSG: string = "Invalid email";
    private readonly EMAIL_ALREADY_EXISTS_ERROR_MSG: string = "Account with this email exists";
    private readonly INVALID_EMAIL_FIREBASE_MSG: string = "The email address is already in use by another account.";
    private readonly INVALID_NAME_ERROR_MSG: string = "Invalid name";
    private readonly INVALID_PASSWORD_ERROR_MSG: string = "Provided value cannot be your password";
    private readonly INVALID_PHONE_ERROR_MSG: string = "Invalid phone number";
    private readonly PASSWORD_PLACEHOLDER: string = "Enter your password";
    private readonly PHONE_PLACEHOLDER: string = "Enter your phone number";
    private readonly NAME_PLACEHOLDER: string = "Enter your name";
    private errorEmailMsg: string = "";
    private errorPasswordMsg: string = "";
    private errorNameMsg: string = "";
    private errorPhoneMsg: string = "";

    private styles = StyleSheet.create({
        textForm: {
            width: 300,
            borderWidth: 1,
            height: 50,
            borderColor: "black",
            backgroundColor: "white",
            marginVertical: 5
        },
        container: {
            flex: 1,
            alignItems: 'center',
            alignContent: 'center',
            justifyContent: 'center',
            backgroundColor: 'rgb(234,233,233)'
        },

        errorMessage: {
            color: "red",
        }
    })
    state = {
        email: "",
        name: "",
        password: "",
        phone: "",
        emailValidated: true,
        nameValidated: true,
        passwordValidated: true,
        phoneValidated: true
    };

    render() {
        return (
            <View style={this.styles.container}>
                <Text style={{
                    fontWeight: "800",
                    fontSize: 30
                }}>Create Your Account</Text>
                <View style={{marginVertical: 40}}>
                    <Text style={this.styles.errorMessage}>{this.state.emailValidated ? "" : this.errorEmailMsg}</Text>
                    <Text>Email:</Text>
                    <TextInput value={this.state.email}
                               onChangeText={text => {
                                   this.onChangeEmail(text)
                               }}
                               placeholder={this.EMAIL_PLACEHOLDER} style={this.styles.textForm}
                               onBlur={() => {
                                   this.validateEmail()
                               }}/>
                    <Text style={this.styles.errorMessage}>{this.state.nameValidated ? "" : this.errorNameMsg}</Text>
                    <Text>Name:</Text>
                    <TextInput value={this.state.name} placeholder={this.NAME_PLACEHOLDER}
                               style={this.styles.textForm}
                               onBlur={() => {
                                   this.validateName()
                               }}
                               onChangeText={text => {
                                   this.onChangeName(text)
                               }}
                    />
                    <Text style={this.styles.errorMessage}>{this.state.phoneValidated ? "" : this.errorPhoneMsg}</Text>
                    <Text>Phone Number:</Text>
                    <TextInput value={this.state.phone} placeholder={this.PHONE_PLACEHOLDER} keyboardType='numeric'
                               style={this.styles.textForm}
                               onBlur={() => {
                                   this.validatePhoneNumber()
                               }}
                               onChangeText={text => {
                                   this.onChangePhoneNumber(text)
                               }}
                    />
                    <Text
                        style={this.styles.errorMessage}>{this.state.passwordValidated ? "" : this.errorPasswordMsg}</Text>
                    <Text>Password:</Text>
                    <TextInput value={this.state.password} style={this.styles.textForm}
                               placeholder={this.PASSWORD_PLACEHOLDER} secureTextEntry
                               onBlur={() => {
                                   this.validatePassword()
                               }}
                               onChangeText={text => {
                                   this.onChangePassword(text)
                               }}
                    />
                    <Button title={"Sign Up"} onPress={() => this.onPressSignUp()}/>
                    <Text style={{
                        alignSelf: "center",
                        marginVertical: 10
                    }}>If you already have account </Text>
                    <Text onPress={() => {
                        this.onPressSignIn()
                    }} style={{
                        color: "darkblue",
                        alignSelf: "center",
                    }}>Sign in</Text>
                </View>
            </View>);
    }

    private onPressSignIn() {
        this.props.navigation.navigate("Sign In")
    }

    private onPressSignUp(): void {
        this.validatePassword();
        this.validatePhoneNumber();
        this.validateEmail();
        this.validateName();
        if (this.state.passwordValidated && this.state.nameValidated && this.state.emailValidated && this.state.phoneValidated) {
            this.setState({
                isLoading: true,
            });
            firebase
                .auth()
                .createUserWithEmailAndPassword(this.state.email.trim(), this.state.password)
                .then((res: any) => {
                    res.user.updateProfile({
                        displayName: this.state.name,
                        phone: this.state.phone
                    });
                    firebase
                        .auth()
                        .signInWithEmailAndPassword(this.state.email.trim(), this.state.password)
                        .then((res: any) => {
                            this.props.navigation.reset({
                                index: 0,
                                routes: [{name: "Home"}],
                            });
                        })
                        .catch((error: Error) => {
                            Alert.alert(
                                "Error",
                                error.message,
                                [
                                    {text: "OK", onPress: () => this.props.navigation.navigate("Sign In")}
                                ],
                                {cancelable: true}
                            );
                        });
                    this.setState({
                        email: "",
                        password: "",
                        name: "",
                        phone: ""
                    });
                }).catch((error: Error) => {
                    console.log(error.message);
                    if (error.message === this.INVALID_EMAIL_FIREBASE_MSG) {
                        this.errorEmailMsg = this.EMAIL_ALREADY_EXISTS_ERROR_MSG;
                        this.setState({
                            emailValidated: false
                        });
                    }
                }
            )
        }
    }

    private onChangeEmail(text: string): void {
        this.setState({email: text});
    }

    private onChangeName(text: string): void {
        this.setState({name: text});
    }

    private onChangePassword(text: string): void {
        this.setState({password: text});
    }

    private onChangePhoneNumber(text: string) {
        this.setState({phone: text});
    }

    private validateEmail(): void {
        if (this.state.email === "") {
            this.errorEmailMsg = this.EMPTY_FIELD_ERROR_MSG;
            this.setState({emailValidated: false});
        } else {
            this.errorEmailMsg = this.INVALID_EMAIL_ERROR_MSG;
            const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
            this.setState({emailValidated: regex.test(this.state.email.trim())});
        }
    }

    private validateName(): void {
        const regex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/g
        if (this.state.name.trim().length < 1) {
            this.setState({nameValidated: false});
            this.errorNameMsg = this.EMPTY_FIELD_ERROR_MSG;
            return
        }
        if (!regex.test(this.state.name)) {
            this.errorNameMsg = this.INVALID_NAME_ERROR_MSG;
            this.setState({nameValidated: false});

        } else {
            this.setState({nameValidated: true});
        }
    }

    private validatePassword(): void {
        if (this.state.password.length < 8) {
            this.setState({passwordValidated: false});
            this.errorPasswordMsg = this.INVALID_PASSWORD_ERROR_MSG;
        } else {
            this.setState({passwordValidated: true});
        }
    }

    private validatePhoneNumber(): void {
        const regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
        if (!regex.test(this.state.phone)) {
            if (this.state.phone === "") {
                this.errorPhoneMsg = this.EMPTY_FIELD_ERROR_MSG;
            } else {
                this.errorPhoneMsg = this.INVALID_PHONE_ERROR_MSG;
            }
            this.setState({phoneValidated: false});
        } else {
            this.setState({phoneValidated: true});
        }
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }


}
