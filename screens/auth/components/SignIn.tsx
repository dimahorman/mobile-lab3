import React, {Component} from "react";
import {Alert, Button, StyleSheet, Text, TextInput, View} from "react-native";
import firebase from "../../../config/firebase";
import {useTheme} from "@react-navigation/native";

export class SignIn extends Component {
    private readonly EMAIL_PLACEHOLDER: string = "Enter your Email";
    private readonly PASSWORD_PLACEHOLDER: string = "Enter your password";
    private readonly EMPTY_FIELD_ERROR_MSG: string = "This field cannot be empty"
    private readonly INVALID_EMAIL_ERROR_MSG: string = "Invalid email";
    private readonly INVALID_PASSWORD_ERROR_MSG: string = "Provided password is invalid";
    private readonly INVALID_PASSWORD_FIREBASE_MSG: string = "The password is invalid or the user does not have a password.";
    private readonly INVALID_EMAIL_FIREBASE_MSG: string = "There is no user record corresponding to this identifier. The user may have been deleted.";

    private errorEmailMsg: string = "";
    private errorPasswordMsg: string = "";
    private styles = StyleSheet.create({
        textForm: {
            width: 300,
            height: 50,
            borderWidth: 1,
            borderColor: "black",
            backgroundColor: "white",
            marginVertical: 5
        },

        container: {
            flex: 1,
            alignItems: 'center',
            alignContent: 'center',
            justifyContent: 'center',
            backgroundColor: 'rgb(234,233,233)'
        },

        errorMessage: {
            color: "red",
        }
    })

    state = {
        email: "",
        password: "",
        passwordValidated: true,
        emailValidated: true
    }

    render() {
        return (
            <View style={this.styles.container}>
                <Text style={{
                    fontWeight: "800",
                    fontSize: 30
                }}>Log in</Text>
                <View>
                    <Text style={this.styles.errorMessage}>{this.state.emailValidated ? "" : this.errorEmailMsg}</Text>
                    <Text>Email:</Text>
                    <TextInput style={this.styles.textForm} placeholder={this.EMAIL_PLACEHOLDER}
                               value={this.state.email}
                               onChangeText={text => {
                                   this.onChangeEmail(text)
                               }}/>
                    <Text
                        style={this.styles.errorMessage}>{this.state.passwordValidated ? "" : this.errorPasswordMsg}</Text>
                    <Text>Password:</Text>
                    <TextInput style={this.styles.textForm} placeholder={this.PASSWORD_PLACEHOLDER} secureTextEntry
                               value={this.state.password}
                               onChangeText={text => {
                                   this.onChangePassword(text)
                               }}
                    />
                    <Button title={"Sign In"} onPress={() => this.onPressSignIn()}/>
                    <Text style={{
                        color: "darkblue",
                        alignSelf: "center",
                        marginVertical: 10
                    }} onPress={() => this.onPressSignUp()}>Or Sign up</Text>

                </View>
            </View>)
    }

    private onPressSignIn(): void {
        this.validateEmail();
        this.validatePassword();
        if (this.state.emailValidated && this.state.passwordValidated) {
            firebase
                .auth()
                .signInWithEmailAndPassword(this.state.email.trim(), this.state.password)
                .then((res: any) => {
                    this.setState({
                        email: "",
                        password: "",
                        emailValidated: true,
                        passwordValidated: true,
                    });
                    this.props.navigation.reset({
                        index: 0,
                        routes: [{name: "User"}],
                    });
                })
                .catch((error: Error) => {
                    console.log(error.message)
                    if (error.message === this.INVALID_PASSWORD_FIREBASE_MSG) {
                        this.errorPasswordMsg = this.INVALID_PASSWORD_ERROR_MSG;
                        this.setState({passwordValidated: false})
                    } else if (error.message === this.INVALID_EMAIL_FIREBASE_MSG) {
                        this.setState({emailValidated: false})
                        this.errorEmailMsg = this.INVALID_EMAIL_ERROR_MSG;
                    }
                });
        }
    }

    private onPressSignUp(): void {
        this.props.navigation.navigate('Sign Up')
    }

    private onChangeEmail(text: string): void {
        this.setState({email: text});
    }

    private onChangePassword(text: string): void {
        this.setState({password: text});
    }

    private validateEmail(): void {
        if (this.state.email === "") {
            this.errorEmailMsg = this.EMPTY_FIELD_ERROR_MSG;
            this.setState({emailValidated: false});
        } else {
            this.errorEmailMsg = this.INVALID_EMAIL_ERROR_MSG;
            const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            this.setState({emailValidated: regex.test(this.state.email.trim())});
        }
    }

    private validatePassword(): void {
        if (this.state.password.length < 8) {
            this.setState({passwordValidated: false});
            this.errorPasswordMsg = this.INVALID_PASSWORD_ERROR_MSG;
        } else {
            this.setState({passwordValidated: true});
        }
    }

    componentWillMount() {
        firebase.auth().onAuthStateChanged((user: any) => {
            if (user) {
                this.props.navigation.reset({
                    index: 0,
                    routes: [{name: "User"}],
                });
            }
        })
    }
}
