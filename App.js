import {StatusBar} from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {DarkTheme, DefaultTheme, NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import {AuthNavigator} from "./screens/auth/AuthNavigator";
import UserNavigator from "./screens/user/UserNavigator";
import firebase from "./config/firebase";
import { Dimensions } from 'react-native';

const db = firebase.firestore();

const MyLightTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: 'rgb(45,157,255)',
        background: 'rgb(234,233,233)',
        card: 'rgb(255,255,255)'
    },
};

const MyDarkTheme = {
    ...DarkTheme,
    colors: {
        ...DarkTheme.colors,
        primary: 'rgb(45,157,255)',
    },
};

export default function App() {
    let onSnapshotUnsubscribe;
    const [theme, setTheme] = useState()

    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                db.collection("app-db")
                    .doc(user.uid)
                    .get()
                    .then((snapshot) => {
                        const data = snapshot.data()
                        if (data === undefined) {
                            db.collection("app-db")
                                .doc(user.uid).set({
                                isThemeSwitched: false
                            });
                        } else {
                            if (snapshot.data().isThemeSwitched) {
                                setTheme(MyDarkTheme)
                            } else {
                                setTheme(MyLightTheme)
                            }
                        }
                    });

                onSnapshotUnsubscribe =
                    db.collection("app-db")
                        .doc(user.uid)
                        .onSnapshot((snapshot) => {
                            const data = snapshot.data()

                            if (data !== undefined) {
                                if (data.isThemeSwitched) {
                                    setTheme(MyDarkTheme)
                                } else {
                                    setTheme(MyLightTheme)
                                }
                            }
                        });
            }
        });
        return () => {
            if (onSnapshotUnsubscribe !== undefined) {
                onSnapshotUnsubscribe();
            }
        }
    }, []);
    return (
        <NavigationContainer theme={theme}>
            <Stack.Navigator headerMode={"none"}>
                <Stack.Screen name="Auth" component={AuthNavigator}/>
                <Stack.Screen name="User" component={UserNavigator}/>
            </Stack.Navigator>

        </NavigationContainer>

    );
}

const Stack = createStackNavigator();

export class StyleSheetFactory {
    static getSheet(colors) {
        return StyleSheet.create({
            container: {
                flex: 1,
                marginTop: StatusBar.currentHeight || 0,
            },

            sideBar: {
                marginTop: 50,
            },

            item: {
                backgroundColor: colors.card,
                padding: 20,
                marginVertical: 8,
                marginHorizontal: 16,
            },
            title: {
                fontSize: 32,
                color: colors.text
            },
            itemDetails: {
                flex: 1,
                alignItems: 'center',
                alignContent: 'center',
                justifyContent: 'center',
            },

            detailText: {
                flexDirection: "row",
            },
            detailHeader: {
                fontWeight: "800",
                color: colors.text
            },
            detailValue: {
                color: colors.text
            },
            logoutButton: {
                bottom: 0
            },
            settingsContainer: {
                flex: 1,
                alignItems: 'center',
                alignContent: 'center',
                justifyContent: 'center',
            },
            roundButton: {
                width: 80,
                height: 80,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 10,
                borderRadius: 40,
                backgroundColor: 'orange',
                position: 'absolute',
                left: Dimensions.get('window').width - 100,
                top: Dimensions.get('window').height - 140
            },
            inputContainer: {
                flex: 1,
                alignItems: 'center',
                alignContent: 'center',
                justifyContent: 'center',
            },
            textForm: {
                width: 300,
                borderWidth: 1,
                height: 50,
                borderColor: colors.text,
                color: colors.text,
                backgroundColor: colors.card,
                marginVertical: 5
            },
            text: {
                color: colors.text
            }
        })
    }
}
